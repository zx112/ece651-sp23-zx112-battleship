package edu.duke.zx112.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BoardTextViewTest {
//    @Test
//    public void test_display_empty_2by2() {
//        Board b1 = new BattleShipBoard(2, 2);
//        BoardTextView view = new BoardTextView(b1);
//        String expectedHeader= "  0|1\n";
//        assertEquals(expectedHeader, view.makeHeader());
//        String expected=
//                expectedHeader+
//                        "A  |  A\n"+
//                        "B  |  B\n"+
//                        expectedHeader;
//        assertEquals(expected, view.displayMyOwnBoard());
//    }
    @Test
    public void test_invalid_board_size() {
        Board<Character> wideBoard = new BattleShipBoard<>(11,20,'X');
        Board<Character> tallBoard = new BattleShipBoard<>(10,27,'X');
        //you should write two assertThrows here

        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(tallBoard));
    }
    private void emptyBoardHelper(int w, int h, String expectedHeader, String expectedBody){
        Board<Character> b1 = new BattleShipBoard<>(w, h,'X');
        BoardTextView view = new BoardTextView(b1);
        assertEquals(expectedHeader, view.makeHeader());
        String expected = expectedHeader + expectedBody + expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }
    public void test_display_empty_3by5() {
        String expectedHeader = "  0|1|2\n";
        String expectedBody =
                "A  | |  A\n" +
                        "B  | |  B\n" +
                        "C  | |  C\n" +
                        "D  | |  D\n" +
                        "E  | |  E\n";
        emptyBoardHelper(3, 5, expectedHeader, expectedBody);
    }




}
