package edu.duke.zx112.battleship;

import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;
public class ZShapeShipTest {
    @Test
    public void testMakeCoord() {
        Placement p = new Placement(new Coordinate(3, 4), 'U');
        HashSet<Coordinate> expected = new HashSet<>();
        expected.add(new Coordinate(4, 4));
        expected.add(new Coordinate(3, 4));
        expected.add(new Coordinate(5, 4));
        expected.add(new Coordinate(5, 5));
        expected.add(new Coordinate(6, 5));
        expected.add(new Coordinate(7, 5));
        assertEquals(expected, ZShapeShip.makeCoord(p));
    }

    @Test
    public void testZShapeShip() {
        ZShapeShip<Integer> ship = new ZShapeShip<>("Z", new Placement(new Coordinate(3, 4), 'U'), 1, 2);
        assertEquals("Z", ship.getName());
    }
}
