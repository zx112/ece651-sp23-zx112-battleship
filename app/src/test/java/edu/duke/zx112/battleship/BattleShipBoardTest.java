package edu.duke.zx112.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BattleShipBoardTest {
    @Test
    public void test_width_and_height() {
//        BattleShipBoard b1 = new BattleShipBoard(10, 20);
//        assertEquals(10, b1.getWidth());
//        assertEquals(20, b1.getHeight());

        Board<Character> b1 = new BattleShipBoard<>(10, 20,'X');
        assertEquals(10, b1.getWidth());
        assertEquals(20, b1.getHeight());

    }

    @Test
    public void test_invalid_dimensions() {
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<>(10, 0,'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<>(0, 20,'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<>(10, -1,'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<>(-8, 20,'X'));
    }

    private <T> void checkWhatIsAtBoard(BattleShipBoard<T> b, T[][] expected) {
        int width = b.getWidth();
        int height = b.getHeight();

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                T target = b.whatIsAt(new Coordinate(i, j));
                char checker = ' ';
                if (target != null) {
                    checker = (char) target;
                }
                assertEquals(checker, expected[i][j]);
            }
        }





    }
//    @Test
//    public void test_whatIsAt() {
//        Character [][] expected = new Character[2][2];
//        BattleShipBoard<Character> board1 = new BattleShipBoard<>(2, 2,'X');
//        checkWhatIsAtBoard(board1, expected);
//        // add ship to (1, 1)
//        board1.tryAddShip(new RectangleShip<Character> (new Coordinate(1,1), 's', '*'));
//        checkWhatIsAtBoard(board1,new Character[][]{{null,null},
//                {null,'s'}});
//        String s = board1.tryAddShip(new RectangleShip<Character> (new Coordinate(1,1), 's', '*'));
//        assertNull(s);
//        checkWhatIsAtBoard(board1,new Character[][]{{null,null},
//                {null,'s'}});
//    }

    @Test
    void testTryAddShip() {
        // test no collision
        PlacementRuleChecker<Character> ruleChecker = new NoCollisionRuleChecker<>(new InBoundsRuleChecker<>(null));
        V1ShipFactory vs1 = new V1ShipFactory();

        Placement p1 = new Placement(new Coordinate(1, 2), 'H');
        Placement p2 = new Placement(new Coordinate(2, 2), 'H');
        Ship<Character> destroyer1 = vs1.makeDestroyer(p1);
        Ship<Character> destroyer2 = vs1.makeDestroyer(p1);
        Ship<Character> battleship = vs1.makeBattleship(p2);
        BattleShipBoard<Character> board = new BattleShipBoard<>(10,10, ruleChecker,'X');
        assertNull(board.tryAddShip(battleship));
        assertNull(board.tryAddShip(destroyer1));
        assertNotNull(board.tryAddShip(destroyer2));
    }

    /**
     * test for fire at for task 19
     */
    @Test
    public void test_fire_at(){
        //initialize the board and ship
        Board<Character> board = new BattleShipBoard<>(5,5,'X');
        V1ShipFactory v1s = new V1ShipFactory();
        Ship<Character> s1 = v1s.makeBattleship(new Placement("A0V"));
        String isAdd = board.tryAddShip(s1);
        //hit it
        Ship<Character> hit  = board.fireAt(new Coordinate("A0"));
        assertSame(hit,s1);//should be the same

    }
    /**
     * test for what is for
     */
    @Test
    public void test_whatisfor(){
        //use the code above
        Board<Character> board = new BattleShipBoard<>(5,5,'X');
        V1ShipFactory v1s = new V1ShipFactory();
        Ship<Character> s1 = v1s.makeBattleship(new Placement("A0V"));
        String isAdd = board.tryAddShip(s1);
        //hit it
        Ship<Character> hit  = board.fireAt(new Coordinate("A0"));
        //for enemy board
        assertEquals('b', board.whatIsAtForEnemy(new Coordinate(0,0)));
        //for our board
        assertEquals('*', board.whatIsAtForSelf(new Coordinate(0,0)));
    }

    /**
     * test for islose
     *
     */

    @Test
    public void test_isLose(){
        //use the code above
        Board<Character> board = new BattleShipBoard<>(5,5,'X');
        V1ShipFactory v1s = new V1ShipFactory();
        Ship<Character> s1 = v1s.makeSubmarine(new Placement("A0V"));
        String isAdd = board.tryAddShip(s1);

        //sunk it
        board.fireAt(new Coordinate("A0"));
        board.fireAt(new Coordinate("B0"));

        //check
        assertTrue(board.isLose());

    }


}