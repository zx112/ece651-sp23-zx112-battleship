package edu.duke.zx112.battleship;
import org.checkerframework.checker.units.qual.C;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


/**
 * check both in bound and no collision
 */

public class NoCollisionRuleCheckerTest {
    @Test
    public void test_both(){
        //initialize the checker and ships and board
        PlacementRuleChecker<Character> checker_collision = new NoCollisionRuleChecker<>(null);
        PlacementRuleChecker<Character> checker_inbound = new InBoundsRuleChecker<>(null);
        V1ShipFactory v1s = new V1ShipFactory();
        Placement p1 = new Placement(new Coordinate(1,1), 'V');
        Placement p3 = new Placement(new Coordinate(2,1),'V');
        Placement p4 = new Placement(new Coordinate(5,1),'V');
        Placement p2 = new Placement(new Coordinate(6,6),'V');
        BattleShipBoard<Character> board = new BattleShipBoard<>(5,5,'X');
        Ship<Character> s = v1s.makeSubmarine(p1);//inbound
        Ship<Character> s1 = v1s.makeSubmarine(p2);//outbound
        assertNull(checker_inbound.checkPlacement(s,board));
        assertNotNull(checker_inbound.checkPlacement(s1,board));
        //add inbound ships
        board.tryAddShip(s);

        //collision test
        Ship<Character> s2 = v1s.makeSubmarine(p3);//collide with s
        Ship<Character> s3 = v1s.makeSubmarine(p4);//not collide
//        board.tryAddShip(s3);
        //assertNull(checker_collision.checkPlacement(s3,board));
        //assertNotNull(checker_collision.checkPlacement(s2,board));//should be false

    }

    /**
     * test for task15
     */
    @Test
    public void test_task15(){
        //initialize the checker and ships and board

        PlacementRuleChecker<Character> checker_inbound = new InBoundsRuleChecker<>(null);
        PlacementRuleChecker<Character> checker_collision = new NoCollisionRuleChecker<>(checker_inbound);
        V1ShipFactory v1s = new V1ShipFactory();
        Placement p1 = new Placement(new Coordinate(1,1), 'V');
        Placement p3 = new Placement(new Coordinate(2,1),'V');
        Placement p2 = new Placement(new Coordinate(6,6),'V');
        BattleShipBoard<Character> board = new BattleShipBoard<>(5,5,'X');
        Ship<Character> s = v1s.makeSubmarine(p1);//inbound
        Ship<Character> s1 = v1s.makeSubmarine(p2);//outbound
        Ship<Character> s2 = v1s.makeSubmarine(p3);//collide with s

        //add inbound ships
        board.tryAddShip(s);
        //assertNotNull(checker_collision.checkPlacement(s2,board));
        //assertNotNull(board.tryAddShip(s2));
    }




}
