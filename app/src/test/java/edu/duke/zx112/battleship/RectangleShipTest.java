package edu.duke.zx112.battleship;

import org.checkerframework.checker.units.qual.C;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import java.util.*;
public class RectangleShipTest {
    @Test
    public void test_MakeCoords() {
        Coordinate upperLeft = new Coordinate(2, 3);
        int width = 2;
        int height = 3;
        HashSet<Coordinate> expected = new HashSet<>();
        expected.add(new Coordinate(2, 3));
        expected.add(new Coordinate(2, 4));
        expected.add(new Coordinate(3, 3));
        expected.add(new Coordinate(3, 4));
        expected.add(new Coordinate(4, 3));
        expected.add(new Coordinate(4, 4));
        HashSet<Coordinate> actual = RectangleShip.makeCoords(upperLeft, width, height);
        assertEquals(expected, actual);
    }

    @Test
    public void test_occupy(){
        Coordinate upperleft = new Coordinate(1,2);
        int width = 2;
        int height = 3;
        //initilize the ship
        SimpleShipDisplayInfo<Character> displayInfo = new SimpleShipDisplayInfo<>('s', '*');
        RectangleShip<Character> ship = new RectangleShip<>("submarine",new Coordinate(1, 2), 2, 3, displayInfo,new SimpleShipDisplayInfo<>(null,'s'));

        //test cases
        assertTrue(ship.occupiesCoordinates(new Coordinate(1,3)));
        assertFalse(ship.occupiesCoordinates(new Coordinate(10,8)));
    }

    @Test
    public void test_wasHitAt_and_recordHitAt(){
        //initialize the ship
        SimpleShipDisplayInfo<Character> displayInfo = new SimpleShipDisplayInfo<>('s', '*');
        RectangleShip<Character> ship = new RectangleShip<>("submarine",new Coordinate(2, 3), 2, 2, displayInfo,new SimpleShipDisplayInfo<>(null,'s'));
        //make coordinates
        Coordinate c_hit1 = new Coordinate(2,4);
        Coordinate c_hit2 = new Coordinate(3,4);
        Coordinate c_unhit = new Coordinate(3,3);
        //hit them!!!
        ship.recordHitAt(c_hit1);
        ship.recordHitAt(c_hit2);
        //check
        assertTrue(ship.wasHitAt(c_hit1));
        assertTrue(ship.wasHitAt(c_hit2));
        //unihit
        assertFalse(ship.wasHitAt(c_unhit));
        //invalid corrdinate
        Coordinate c_invalid = new Coordinate(1,1);
        assertThrows(IllegalArgumentException.class, () -> ship.wasHitAt(c_invalid));
    }

    @Test
    public void test_isSunk(){
        //initialize our ship
        SimpleShipDisplayInfo<Character> displayInfo = new SimpleShipDisplayInfo<>('s','*');
        RectangleShip<Character> ship = new RectangleShip<>("submarine",new Coordinate(2,3),2,2,displayInfo,new SimpleShipDisplayInfo<>(null,'s'));
        //create coordinates to sink the ship
        ship.recordHitAt(new Coordinate(2,3));
        ship.recordHitAt(new Coordinate(2,4));
        ship.recordHitAt(new Coordinate(3,3));
        ship.recordHitAt(new Coordinate(3,4));
        assertTrue(ship.isSunk());

        //initialize a ship didn't sunk
        RectangleShip<Character> ship1 = new RectangleShip<>("submarine",new Coordinate(1,1),2,2,displayInfo,new SimpleShipDisplayInfo<>(null,'s'));
        //create coordinates to sink the ship
        ship1.recordHitAt(new Coordinate(1,1));
        assertFalse(ship1.isSunk());
    }

    @Test
    public void test_getDisplayInfoAt(){
        //initialize the ship
        SimpleShipDisplayInfo<Character> displayInfo = new SimpleShipDisplayInfo<>('s','*');
        RectangleShip<Character> ship = new RectangleShip<>("submarine",new Coordinate(1,1),1,2,displayInfo,new SimpleShipDisplayInfo<>(null,'s'));
        //hit one coordinate
        ship.recordHitAt(new Coordinate(1,1));
        assertEquals('*',ship.getDisplayInfoAt(new Coordinate(1,1),true));
        assertEquals('s',ship.getDisplayInfoAt(new Coordinate(2,1),true));
    }

    /**
     * test if we get all the coordinates from a ship
     */
    @Test
    public void test_getCoordinates(){
        //initialize our ship
        SimpleShipDisplayInfo<Character> displayInfo = new SimpleShipDisplayInfo<>('s', '*');
        //should have (1,1) (2,1) (3,1)
        RectangleShip<Character> s = new RectangleShip<>("submarine", new Coordinate(1, 1), 1, 3, displayInfo, new SimpleShipDisplayInfo<>(null,'s'));
        //initialize our sets
        Set<Coordinate> expect = new HashSet<>();
        //fill in expect
        expect.add(new Coordinate(1,1));
        expect.add(new Coordinate(2,1));
        expect.add(new Coordinate(3,1));
        //fill in real output
        assertEquals(expect, s.getCoordinates());


    }
    @Test
    public void testRectangleShipConstructor1() {
        Coordinate upperLeft = new Coordinate(1, 2);
        String name = "testship";
        int width = 1;
        int height = 2;
        String data = "s";
        String onHit = "*";

        RectangleShip<String> ship = new RectangleShip<>(name, upperLeft, width, height, data, onHit);
        assertEquals(name, ship.getName());
        assertEquals(upperLeft, new Coordinate(1, 2));
        assertEquals(width, 1);
        assertEquals(height, 2);
    }

    @Test
    public void testRectangleShipConstructor2() {
        Coordinate upperLeft = new Coordinate(5, 6);
        String data = "s";
        String onHit = "*";

        RectangleShip<String> ship = new RectangleShip<>(upperLeft, data, onHit);
        assertEquals("testship", ship.getName());
        assertEquals(upperLeft, new Coordinate(5, 6));
        assertEquals(1, 1);
        assertEquals(1, 1);
    }

}
