package edu.duke.zx112.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class V1ShipFactoryTest {
    @Test
    void testCreateShip() {
        //initialize the ship factory
        V1ShipFactory vs1 = new V1ShipFactory();
        Placement p1 = new Placement("A2H");
        Ship<Character> c = vs1.makeCarrier(p1);
        char expectedLetter = 'c';
        assertEquals(expectedLetter,c.getDisplayInfoAt(p1.getWhere(),true));
        Ship<Character> d = vs1.makeDestroyer(p1);
        char expectedLetter1 = 'd';
        assertEquals(expectedLetter1,d.getDisplayInfoAt(p1.getWhere(),true));
        Ship<Character> s = vs1.makeSubmarine(p1);
        char expectedLetter2 = 's';
        assertEquals(expectedLetter2,s.getDisplayInfoAt(p1.getWhere(),true));
        Ship<Character> b = vs1.makeBattleship(p1);
        char expectedLetter3 = 'b';
        assertEquals(expectedLetter3,b.getDisplayInfoAt(p1.getWhere(),true));

    }

}

