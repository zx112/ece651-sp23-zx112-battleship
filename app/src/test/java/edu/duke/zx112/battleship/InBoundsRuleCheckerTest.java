package edu.duke.zx112.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class InBoundsRuleCheckerTest {
  @Test
  public void test_InBoundsRuleChecker() {
    //create ship
    V1ShipFactory v1s = new V1ShipFactory();
    Placement p = new Placement(new Coordinate(1,1),'V');
    Placement p1 = new Placement(new Coordinate(5,5),'V');//out of bound
    Ship<Character> s = v1s.makeSubmarine(p);
    Ship<Character> d = v1s.makeDestroyer(p1);
    //create the board
    BattleShipBoard<Character> board = new BattleShipBoard<>(4,4,'X');
    //create the checker
    PlacementRuleChecker<Character> checker = new InBoundsRuleChecker<>(null);
    assertNull(checker.checkMyRule(s, board));
    assertNotNull(checker.checkMyRule(d, board));

  }

}
