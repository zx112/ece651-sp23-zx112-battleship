package edu.duke.zx112.battleship;

import org.junit.jupiter.api.Test;

import java.io.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TextPlayerTest {
    private TextPlayer createTextPlayer(int w, int h, String inputData, OutputStream bytes) {
        BufferedReader input = new BufferedReader(new StringReader(inputData));
        PrintStream output = new PrintStream(bytes, true);
        Board<Character> board = new BattleShipBoard<Character>(w, h,'X');
        BoardTextView view = new BoardTextView(board);
        V2ShipFactory shipFactory1 = new V2ShipFactory();
        V1ShipFactory shipFactory = new V1ShipFactory();
        return new TextPlayer("A", board, view, input, output, shipFactory,shipFactory1);
    }
    @Test
    void test_read_placement() throws IOException {
        // construct it with a String, and then read from it like an input stream (e.g. stringstream
        StringReader sr = new StringReader("B2V\nC8H\na4v\n");
        // to collect the output. and we can wrap it in a PrintStream
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(bytes, true);
        //App now reads from sr (our StringReader) and writes to ps
        // (our PrintStream that writes to our ByteArrayOutputStream
        Board<Character> b = new BattleShipBoard<>(10, 20,'X');
        BoardTextView view = new BoardTextView(b);
        TextPlayer player = new TextPlayer("A", b, view, new BufferedReader(sr), ps, new V1ShipFactory(),new V2ShipFactory());

        String prompt = "Please enter a location for a ship: ";
        Placement[] expected = new Placement[3];
        expected[0] = new Placement(new Coordinate(1, 2), 'V');
        expected[1] = new Placement(new Coordinate(2, 8), 'H');
        expected[2] = new Placement(new Coordinate(0, 4), 'V');

        for (Placement placement : expected) {
            Placement p = player.readPlacement(prompt);
            assertEquals(p, placement); //did we get the right Placement back
            assertEquals(prompt + "\n", bytes.toString()); //should have printed prompt and newline
            bytes.reset(); //clear out bytes for next time around
        }
    }
    @Test
    void test_do_placement() throws IOException{
        String prompt = "Player A where do you want to place a Submarine?";
        StringReader sr = new StringReader("B2v");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(bytes, true);
        Board<Character> b = new BattleShipBoard<>(10, 20,'X');
        BoardTextView view = new BoardTextView(b);
        TextPlayer player = new TextPlayer("A", b, view, new BufferedReader(sr), ps, new V1ShipFactory(),new V2ShipFactory());

        V1ShipFactory shipFactory = new V1ShipFactory();
        //player.doOnePlacement("Submarine", (p)->shipFactory.makeSubmarine(p));
        String expected = prompt + "\n" +"Current ocean:\n"+
                "  0|1|2|3|4|5|6|7|8|9\n" +
                "A  | | | | | | | | |  A\n" +
                "B  | |s| | | | | | |  B\n" +
                "C  | |s| | | | | | |  C\n" +
                "D  | | | | | | | | |  D\n" +
                "E  | | | | | | | | |  E\n" +
                "F  | | | | | | | | |  F\n" +
                "G  | | | | | | | | |  G\n" +
                "H  | | | | | | | | |  H\n" +
                "I  | | | | | | | | |  I\n" +
                "J  | | | | | | | | |  J\n" +
                "K  | | | | | | | | |  K\n" +
                "L  | | | | | | | | |  L\n" +
                "M  | | | | | | | | |  M\n" +
                "N  | | | | | | | | |  N\n" +
                "O  | | | | | | | | |  O\n" +
                "P  | | | | | | | | |  P\n" +
                "Q  | | | | | | | | |  Q\n" +
                "R  | | | | | | | | |  R\n" +
                "S  | | | | | | | | |  S\n" +
                "T  | | | | | | | | |  T\n" +
                "  0|1|2|3|4|5|6|7|8|9\n"   ;

        assertEquals(expected, bytes.toString());
    }

}
