package edu.duke.zx112.battleship;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

public class TShapeShipTest {

    @Test
    public void makeCoord_up() {
        Placement placement = new Placement(new Coordinate(1, 2), 'U');
        HashSet<Coordinate> expectedCoordinates = new HashSet<>();
        expectedCoordinates.add(new Coordinate(1, 3));
        expectedCoordinates.add(new Coordinate(2, 2));
        expectedCoordinates.add(new Coordinate(2, 3));
        expectedCoordinates.add(new Coordinate(2, 4));

        HashSet<Coordinate> actualCoordinates = TShapeShip.makeCoord(placement);

        assertEquals(expectedCoordinates, actualCoordinates);
    }

    @Test
    public void makeCoord_right() {
        Placement placement = new Placement(new Coordinate(1, 2), 'R');
        HashSet<Coordinate> expectedCoordinates = new HashSet<>();
        expectedCoordinates.add(new Coordinate(1, 2));
        expectedCoordinates.add(new Coordinate(2, 2));
        expectedCoordinates.add(new Coordinate(2, 3));
        expectedCoordinates.add(new Coordinate(3, 2));

        HashSet<Coordinate> actualCoordinates = TShapeShip.makeCoord(placement);

        assertEquals(expectedCoordinates, actualCoordinates);
    }

    @Test
    public void makeCoord_left() {
        Placement placement = new Placement(new Coordinate(1, 2), 'L');
        HashSet<Coordinate> expectedCoordinates = new HashSet<>();
        expectedCoordinates.add(new Coordinate(2, 2));
        expectedCoordinates.add(new Coordinate(1, 3));
        expectedCoordinates.add(new Coordinate(2, 3));
        expectedCoordinates.add(new Coordinate(3, 3));

        HashSet<Coordinate> actualCoordinates = TShapeShip.makeCoord(placement);

        assertEquals(expectedCoordinates, actualCoordinates);
    }

    @Test
    public void makeCoord_down() {
        Placement placement = new Placement(new Coordinate(1, 2), 'D');
        HashSet<Coordinate> expectedCoordinates = new HashSet<>();
        expectedCoordinates.add(new Coordinate(1, 2));
        expectedCoordinates.add(new Coordinate(1, 3));
        expectedCoordinates.add(new Coordinate(1, 4));
        expectedCoordinates.add(new Coordinate(2, 3));

        HashSet<Coordinate> actualCoordinates = TShapeShip.makeCoord(placement);

        assertEquals(expectedCoordinates, actualCoordinates);
    }
    @Test
    public void testGetName() {
        Placement placement = new Placement(new Coordinate(1, 1), 'U');
        TShapeShip<String> ship = new TShapeShip<>("Test Ship", placement, "X", "O");

        assertEquals("Test Ship", ship.getName());
    }

}