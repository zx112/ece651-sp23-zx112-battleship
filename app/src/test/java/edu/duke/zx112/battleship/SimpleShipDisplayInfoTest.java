package edu.duke.zx112.battleship;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class SimpleShipDisplayInfoTest {
    @Test
    public void test_getInfo(){
        SimpleShipDisplayInfo<Character> display = new SimpleShipDisplayInfo<>('x', '*');
        Character info1 = display.getInfo(new Coordinate(1, 1), true);
        Character info2 = display.getInfo(new Coordinate(1, 1), false);
        assertEquals('*', info1);
        assertEquals('x', info2);

    }
}
