package edu.duke.zx112.battleship;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
public class PlacementTest {
    @Test
    public void test_valid_placement(){
        Coordinate c = new Coordinate("A2");
        Placement p1 = new Placement(c, 'V');
        Placement p2 = new Placement(c, 'H');

        assertEquals(p1.getOrientation(), 'V');
        assertEquals(p2.getOrientation(), 'H');

    }
    @Test
    public void test_invalid_placement(){
        Coordinate c = new Coordinate("A2");
        assertThrows(IllegalArgumentException.class,() -> new Placement(c,'x'));
        assertThrows(IllegalArgumentException.class,() -> new Placement(c,'@'));

    }

    @Test
    public void test_toString(){
        Coordinate c1 = new Coordinate(1,2);
        Placement p1 = new Placement(c1, 'H');
        Placement p2 = new Placement(c1, 'V');
        assertEquals(p1.toString(), "(1, 2) Horizontal");
        assertEquals(p2.toString(), "(1, 2) Vertical");

    }

    @Test
    public void test_equals(){
        Coordinate c1 = new Coordinate("A1");
        Coordinate c2 = new Coordinate("A1");
        Placement p1 = new Placement(c1, 'H');
        Placement p2 = new Placement(c1, 'V');
        Placement p3 = new Placement(c1, 'H');
        Placement p4 = new Placement(c2, 'H');

        assertEquals(p1, p1);
        assertEquals(p1, p3);//equal
        assertEquals(p1, p4);//equal

        assertNotEquals(p1,p2);
        assertNotEquals(p1,"(1,2)H");

    }
    @Test
    public void test_hashCode(){
        Coordinate c1 = new Coordinate("A1");
        Coordinate c2 = new Coordinate("A1");
        Placement p1 = new Placement(c1, 'H');
        Placement p2 = new Placement(c1, 'V');
        Placement p3 = new Placement(c1, 'H');
        Placement p4 = new Placement(c2, 'H');

        assertEquals(p1.hashCode(), p3.hashCode());
        assertEquals(p1.hashCode(),p4.hashCode());
        assertNotEquals(p1.hashCode(),p2.hashCode());
    }
    @Test
    public void test_StringCon(){
        Coordinate c1 = new Coordinate("A1");
        Coordinate c2 = new Coordinate("C2");
        Placement p1 = new Placement(c1, 'H');//01H
        Placement p2 = new Placement(c1, 'V');//01V
        Placement p3 = new Placement(c1, 'H');//01H
        Placement p4 = new Placement(c2, 'H');//22H

        Placement p5 = new Placement("B1V");//11V

        assertEquals(p1.getWhere().getRow(),0);
        assertEquals(p4.getWhere().getRow(),2);
        assertEquals(p5.getWhere().getRow(),1);
        assertNotEquals(p2.getWhere().getRow(),2);
    }

    @Test
    public void test_invalid_StringCon(){
        Coordinate c1 = new Coordinate("A1");
        //Placement p1 = new Placement("000");
        assertThrows(IllegalArgumentException.class,() -> new Placement(c1,'X'));
        assertThrows(IllegalArgumentException.class,() -> new Placement("000"));
        assertThrows(IllegalArgumentException.class,() -> new Placement("!@#"));
    }
}
