package edu.duke.zx112.battleship;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;
public class V2ShipFactoryTest {
    @Test
    public void testMakeBattleship() {
        Placement where = new Placement("A0U");
        V2ShipFactory factory = new V2ShipFactory();
        Ship<Character> ship = factory.makeBattleship(where);
        assertEquals("Battleship", ship.getName());
        HashSet<Coordinate> set = new HashSet<>();
        set.add(new Coordinate(1,2));
        set.add(new Coordinate(1,1));
        set.add(new Coordinate(0,1));
        set.add(new Coordinate(1,0));
        assertEquals(set,ship.getCoordinates());
    }
    @Test
    public void testMakeCarrier() {
        Placement where = new Placement("A0D");
        V2ShipFactory factory = new V2ShipFactory();
        Ship<Character> ship = factory.makeCarrier(where);
        assertEquals("Carrier", ship.getName());
        HashSet<Coordinate> set = new HashSet<>();
        set.add(new Coordinate(0,0));
        set.add(new Coordinate(1,1));
        set.add(new Coordinate(2,1));
        set.add(new Coordinate(1,0));
        set.add(new Coordinate(2,0));
        set.add(new Coordinate(3,1));
        set.add(new Coordinate(4,1));
        assertEquals(set,ship.getCoordinates());
    }
    @Test
    public void test_error(){
        Placement where = new Placement("A0D");
        V2ShipFactory factory = new V2ShipFactory();
        Ship<Character> ship = factory.makeSubmarine(where);
    }
}
