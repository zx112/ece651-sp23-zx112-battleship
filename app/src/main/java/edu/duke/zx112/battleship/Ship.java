package edu.duke.zx112.battleship;


import java.util.*;

/**
 * This interface represents any type of Ship in our BattleShip game. It is generic
 * in typename T, which is the type of information the view needs to display this ship.
 */
public interface Ship<T> {
    public HashMap<Coordinate, Integer> getmyPieces_v2();
    public List<Integer> getOrder_hit();
    /**

     * Get the name of this Ship, such as "submarine".
     * @return the name of this ship
     */
    public String getName();
    /**
     * Check if this ship occupies the given coordinate.
     * @param where is the Coordinate to check if this Ship occupies
     * @return true if where is inside this ship, false if not.
     */
    boolean occupiesCoordinates(Coordinate where);

    /**
     * Check if this ship has been hit in all of its locations meaning it has been sunk.
     *
     * @return true if this ship has been sunk, false otherwise.
     */
    boolean isSunk();

    /**
     * Make this ship record that is has been hit at the given coordinate. The specified
     * coordinate must be part of the ship.
     * @param where specifies the coordinates that were hit.
     * @throws IllegalArgumentException if where is not part of the Ship.
     */
    void recordHitAt(Coordinate where);

    /**
     * Check if this ship was hit at the specified coordinate. The specified
     * coordinate must be part of the ship.
     * @param where is the coordinates to check.
     * @return true if the ship was hit at the indicated coordinates, and false otherwise.
     * @throws IllegalArgumentException if where is not part of the Ship.
     */
    boolean wasHitAt(Coordinate where);

    /**
     * Return the view-specific information at the given coordinate. This coordinate must
     * be part of the ship.
     * @param where is the coordinate to return information.
     * @throws IllegalArgumentException if where is not part of the Ship.
     * @return the view-specific information at the coordinate.
     */
    public T getDisplayInfoAt(Coordinate where, boolean myShip);
    /**

     * Get all of the Coordinates that this Ship occupies.
     * @return An Iterable with the coordinates that this Ship occupies
     */
    public Iterable<Coordinate> getCoordinates();

    public Ship<T> fireAt(Coordinate c);

    public ArrayList<Integer> markHits();
    public void restoreHits(ArrayList<Integer> hitIndexes);
}
