package edu.duke.zx112.battleship;

public class NoCollisionRuleChecker<T> extends  PlacementRuleChecker<T> {

    public NoCollisionRuleChecker(PlacementRuleChecker next) {
        super(next);
    }

    /**
     * check if a coordinate is occupy by a ship
     * @param theShip
     * @param theBoard
     * @return
     */
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        for (Coordinate c : theShip.getCoordinates()) {
            if (theBoard.whatIsAtForSelf(c) != null) {
                return "That placement is invalid: the ship overlaps another ship.";
            }

        }
        return null;
    }

}
