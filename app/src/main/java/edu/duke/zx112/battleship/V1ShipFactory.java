package edu.duke.zx112.battleship;

/**
 * As a quick reminder the ships are:
 * Submarine:   1x2 's'
 * Destroyer:   1x3 'd'
 * Battleship:  1x4 'b'
 * Carrier:     1x6 'c'
 */
public class V1ShipFactory implements AbstractShipFactory<Character>{

    /**
     * a method to create a ship
     * @param where
     * @param w
     * @param h
     * @param letter
     * @param name
     * @return
     */
    protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name) {
        if (where.getOrientation() == 'V') {
            return new RectangleShip<>(name, where.getWhere(), w, h, new SimpleShipDisplayInfo<>(letter, '*'), new SimpleShipDisplayInfo<>(null,letter),where.getOrientation());
        }
        else  {
            return new RectangleShip<>(name, where.getWhere(), h, w, new SimpleShipDisplayInfo<>(letter, '*'),new SimpleShipDisplayInfo<>(null,letter),where.getOrientation());
        }
    }

    /**
     * four methods to create different ships
     * @param where specifies the location and orientation of the ship to make
     * @return
     */
    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        return createShip(where, 1, 2, 's', "Submarine");
    }

    @Override
    public Ship<Character> makeBattleship(Placement where) {
        return createShip(where, 1, 4, 'b', "Battleship");
    }

    @Override
    public Ship<Character> makeCarrier(Placement where) {
        return createShip(where, 1, 6, 'c', "Carrier");
    }

    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        return createShip(where, 1, 3, 'd', "Destroyer");
    }
}
