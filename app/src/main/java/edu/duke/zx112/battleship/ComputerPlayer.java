package edu.duke.zx112.battleship;

import java.io.*;
import java.util.*;


/**
 * the class to generate computer player
 */

public class ComputerPlayer extends TextPlayer {
    final Random random;

    /**
     * this function initialize a player and pass all the parameters we need
     *
     * @param
     * @param name
     * @param board
     * @param view
     * @param inputReader
     * @param out
     */
    public ComputerPlayer(String name, Board<Character> board, BoardTextView view, BufferedReader inputReader, PrintStream out, V1ShipFactory shipFactory1, V2ShipFactory shipFactory2) {
        super(name, board, view, inputReader, out, shipFactory1, shipFactory2);
        this.random = new Random();
    }

    /**
     * for computer to do one placement
     *
     * @param shipName
     */
    @Override
    public void doOnePlacement(String shipName) {
        // Loop until the ship is successfully placed
        while (true) {
            try {
                // Generate a random row and column on the board
                char row = (char) (this.random.nextInt(20) + 65);
                int col = this.random.nextInt(10);

                // Generate a random orientation for the ship
                int orientation = this.random.nextInt(6); // 6 possible orientations
                char orientationChar;
                switch (orientation) {
                    case 0:
                        orientationChar = 'H'; // horizontal
                        break;
                    case 1:
                        orientationChar = 'V'; // vertical
                        break;
                    case 2:
                        orientationChar = 'U'; // up
                        break;
                    case 3:
                        orientationChar = 'R'; // right
                        break;
                    case 4:
                        orientationChar = 'D'; // down
                        break;
                    default:
                        orientationChar = 'L'; // left
                        break;
                }

                // Create a placement string with the row, column, and orientation
                String placementString = row + String.valueOf(col) + orientationChar;

                // Create a Placement object and a new Ship object based on the ship name and the placement
                Placement placement = new Placement(placementString);
                Ship<Character> ship = shipCreationFns.get(shipName).apply(placement);

                // Try to add the ship to the game board
                String errorMsg = theBoard.tryAddShip(ship);

                // If the ship is successfully placed, break out of the loop and print the current state of the game board
                if (errorMsg == null) {
                    break;
                } else {
                    // If the placement is unsuccessful, print the error message
                    System.out.println(errorMsg);
                }
            } catch (IllegalArgumentException e) {
                // If an IllegalArgumentException is thrown, print the error message
                System.out.println(e.getMessage());
            }
        }

    }

    /**
     * the computer playoneturn, it doesn't have to display the board anymore
     *
     * @param enemyBoard
     * @param enemyName
     */
    @Override
    public void playOneTurn(Board<Character> enemyBoard, String enemyName) {
        String prompt = "Player " + this.name + "'s turn (Computer)";
        System.out.println(prompt);
        playOneTurn_fire(enemyBoard);
    }

    /**
     * for compute fire
     *
     * @param enemyBoard
     */
    public void playOneTurn_fire(Board<Character> enemyBoard) {
        // Initialize the target coordinate
        Coordinate targetCoord;

        // Generate a random target coordinate and keep trying until a valid one is generated
        while (true) {
            try {
                // Generate a random row and column for the target coordinate
                String targetRow = String.valueOf((char) (this.random.nextInt(20) + 65)); // A to T
                String targetCol = String.valueOf(this.random.nextInt(10)); // 0 to 9

                // Concatenate the row and column into a string and create a Coordinate object from it
                String targetStr = targetRow + targetCol;
                targetCoord = new Coordinate(targetStr);

                // If a valid coordinate is created, exit the while loop
                break;
            } catch (IllegalArgumentException ex) {
                // If an invalid coordinate is generated, print an error message and try again
                System.out.println("Invalid target coordinates: " + ex.getMessage());
            }
        }

        // Fire at the target coordinate on the enemy board and check if a ship was hit
        Ship<Character> hitShip = enemyBoard.fireAt(targetCoord);

        // Print a message based on the result of the shot
        if (hitShip == null) {
            // If the shot missed, print a message indicating so
            System.out.println("Computer player missed!");
        } else {
            // If the shot hit a ship, print a message indicating so and the name of the ship that was hit
            System.out.println("Computer player hit a " + hitShip.getName() + "!");
        }
    }
}
