package edu.duke.zx112.battleship;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;

public class App {
    TextPlayer player1;
    TextPlayer player2;
    public App(TextPlayer p1, TextPlayer p2) {
        player1 = p1;
        player2 = p2;
    }

    /**
     * receive int choice from user to choose player or computer
     * @param inputReader
     * @return
     */
    public static int getChoice(BufferedReader inputReader){
        int input;
        while (true) {
            try {
                input = Integer.valueOf(inputReader.readLine());
                if( input != 1 && input != 2){
                    throw new IllegalArgumentException("Please select correct mode for players!(either 1 or 2)");
                } else {
                    break;
                }
            }catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
        return input;
    }
    /**
     * Create a 10x20 board, create an App with that board, and call
     * doOnePlacement on that app.  For the other arguments to App's constructor,
     * we'll pass new InputStreamReader(System.in) for the input, and pass System.out
     * for the output.
     * @param args nothing
     * @throws IOException when encountering some reading and printing errors
     */

    public static void main(String[] args) throws IOException {
        //initialize the variables
        Board<Character> b1 = new BattleShipBoard<>(10, 20,'X');
        Board<Character> b2 = new BattleShipBoard<>(10, 20,'X');
        BoardTextView view1 = new BoardTextView(b1);
        BoardTextView view2 = new BoardTextView(b2);
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        V1ShipFactory factory1 = new V1ShipFactory();
        V2ShipFactory factory2 = new V2ShipFactory();
        //choose the game mode
        String prompt1 = "Please select the mode for player1:\n" + "1 for human player\n" + "2 for computer player\n";
        System.out.println(prompt1);
        int choice1 = getChoice(input);
        TextPlayer player1 = null;
        if (choice1 == 1)
            player1 = new TextPlayer("A", b1, view1, input, System.out, factory1, factory2);
        else if (choice1 ==2) {
            player1 = new ComputerPlayer("A", b1, view1, input, System.out, factory1, factory2);
        }
        String playerchoose = choice1 == 1 ? "player" : "Computer";
        System.out.println("OK, you have choose " + playerchoose +" for player1. next ------------------------");

        String prompt2 = "Please select the mode for player2:\n" + "1 for human player\n" + "2 for computer player\n";
        System.out.println(prompt2);
        int choice2 = getChoice(input);
        TextPlayer player2 = null;
        if (choice2 == 1)
            player2 = new TextPlayer("B", b2, view2, input, System.out, factory1, factory2);
        else if (choice2 ==2) {
            player2 = new ComputerPlayer("B", b2, view2, input, System.out, factory1, factory2);
        }
        //start the game
        App app = new App(player1, player2);
        app.doPlacementPhase();
        app.doAttackPhase();

    }
    void doPlacementPhase() throws IOException {
        player1.doPlacementPhase();
        player2.doPlacementPhase();
    }
    public void doAttackPhase() throws IOException{
        while(true){
            player1.playOneTurn(player2.theBoard, player2.name);
            if(player2.check_lost()){
                System.out.println("Player "+player1.name+" won!");
                break;
            }
            player2.playOneTurn(player1.theBoard, player2.name);
            if(player1.check_lost()){
                System.out.println("Player "+player2.name+" won!");
                break;
            }
        }
    }



}



