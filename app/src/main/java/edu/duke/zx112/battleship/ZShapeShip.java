package edu.duke.zx112.battleship;

import java.util.*;

/**
 * This is a class to make z shape ships, carriers
 */
public class ZShapeShip<T> extends BasicShip<T> {
    //similar the T shape class
    private final String name;

    /**
     *  c                       c
     *                   c           cccc        cc         ccc
     *                   cc   OR    ccc      OR  cc   OR  cccc
     *                   cc                       c
     *                    c                       c
     *
     * Make coordinates for z shape
     * @param placement
     * @return
     */
    public static HashSet<Coordinate> makeCoord(Placement placement){
        //initialize the variables
        Coordinate upperLeft = placement.getWhere();
        Character orientation = placement.getOrientation();
        int row = upperLeft.getRow();
        int column = upperLeft.getColumn();
        HashSet<Coordinate> set = new HashSet<>();//the output

        //catch the exception
        if(orientation.equals('V') || orientation.equals('H')) {
            throw new IllegalArgumentException(
                    "the orientation of this type of ship must be 'U' or 'u' or 'D' or 'd' or 'L' or 'l' or 'R' or 'r' but is "  + orientation
            );
        }
        //generate coordinates
        //up
        if(orientation.equals('U')){
            set.add(new Coordinate(row+1,column));
            set.add(new Coordinate(row,column));
            set.add(new Coordinate(row+2,column));
            set.add(new Coordinate(row+2,column+1));
            set.add(new Coordinate(row+3,column+1));
            set.add(new Coordinate(row+3,column+0));
            set.add(new Coordinate(row+4,column+1));
        }
        //right
        if (orientation.equals('R')){
            set.add(new Coordinate(row,column+1));
            set.add(new Coordinate(row,column+2));
            set.add(new Coordinate(row,column+3));
            set.add(new Coordinate(row,column+4));
            set.add(new Coordinate(row+1,column));
            set.add(new Coordinate(row+1,column+1));
            set.add(new Coordinate(row+1,column+2));
        }
        //left
        if (orientation.equals('L')){
            set.add(new Coordinate(row+1,column));
            set.add(new Coordinate(row+1,column+1));
            set.add(new Coordinate(row+1,column+2));
            set.add(new Coordinate(row+1,column+3));
            set.add(new Coordinate(row,column+2));
            set.add(new Coordinate(row,column+3));
            set.add(new Coordinate(row,column+4));
        }
        //down
        if (orientation.equals('D')) {
            set.add(new Coordinate(row,column));
            set.add(new Coordinate(row+1,column));
            set.add(new Coordinate(row+2,column));
            set.add(new Coordinate(row+1,column+1));
            set.add(new Coordinate(row+2,column+1));
            set.add(new Coordinate(row+3,column+1));
            set.add(new Coordinate(row+4,column+1));
        }
        return set;
    }
    public ZShapeShip(String name, Placement place,
                      SimpleShipDisplayInfo<T> selfDisplayInfo, SimpleShipDisplayInfo<T> enemyDisplayInfo,Character orientation) {
        super(makeCoord(place), selfDisplayInfo, enemyDisplayInfo, makeMapCoord(place.getWhere(),orientation,name));
        this.name = name;
    }

    public ZShapeShip(String name, Placement p, T data, T onHit,Character orientation) {
        this(name, p, new SimpleShipDisplayInfo<>(data, onHit), new SimpleShipDisplayInfo<>(null, data),orientation);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Ship<T> fireAt(Coordinate c) {
        return null;
    }

    /**
     * Creates a HashMap of Coordinates to Integers for a Carrier game piece
     * @param upperLeft the Coordinate of the top-left corner of the Carrier piece
     * @param orientation a character representing the orientation of the Carrier piece ('U' for up, 'R' for right, 'D' for down, 'L' for left)
     * @param name a String representing the name of the Carrier piece
     * @return a HashMap of Coordinates to Integers for the Carrier piece
     */
    static HashMap<Coordinate, Integer> makeMapCoord(Coordinate upperLeft, Character orientation, String name) {
        HashMap<Coordinate, Integer> tmp = new HashMap<>();
        int row = upperLeft.getRow();
        int column = upperLeft.getColumn();
        if (name.equals("Carrier")) {
            if (orientation == 'U' || orientation == 'u') {
                tmp.put(new Coordinate(row, column), 1);
                tmp.put(new Coordinate(row + 1, column), 2);
                tmp.put(new Coordinate(row + 2, column), 3);
                tmp.put(new Coordinate(row + 3, column), 4);
                tmp.put(new Coordinate(row + 2, column + 1), 5);
                tmp.put(new Coordinate(row + 3, column + 1), 6);
                tmp.put(new Coordinate(row + 4, column + 1), 7);
            }
            if (orientation == 'r' || orientation == 'R') {
                tmp.put(new Coordinate(row, column + 4), 1);
                tmp.put(new Coordinate(row, column + 3), 2);
                tmp.put(new Coordinate(row, column + 2), 3);
                tmp.put(new Coordinate(row, column + 1), 4);
                tmp.put(new Coordinate(row + 1, column + 2), 5);
                tmp.put(new Coordinate(row + 1, column + 1), 6);
                tmp.put(new Coordinate(row + 1, column), 7);
            }
            if (orientation == 'd' || orientation == 'D') {
                tmp.put(new Coordinate(row + 4, column + 1), 1);
                tmp.put(new Coordinate(row + 3, column + 1), 2);
                tmp.put(new Coordinate(row + 2, column + 1), 3);
                tmp.put(new Coordinate(row + 1, column + 1), 4);
                tmp.put(new Coordinate(row + 2, column), 5);
                tmp.put(new Coordinate(row + 1, column), 6);
                tmp.put(new Coordinate(row, column), 7);
            }
            if (orientation == 'l' || orientation == 'L') {
                tmp.put(new Coordinate(row + 1, column), 1);
                tmp.put(new Coordinate(row + 1, column + 1), 2);
                tmp.put(new Coordinate(row + 1, column + 2), 3);
                tmp.put(new Coordinate(row + 1, column + 3), 4);
                tmp.put(new Coordinate(row, column + 2), 5);
                tmp.put(new Coordinate(row, column + 3), 6);
                tmp.put(new Coordinate(row, column + 4), 7);
            }


        }
        return tmp;
    }

}



