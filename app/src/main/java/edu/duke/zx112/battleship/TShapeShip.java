package edu.duke.zx112.battleship;

import java.util.*;

/**
 * This is a class to make T shape ships for battleship
 * @param <T>
 */
public class TShapeShip<T> extends BasicShip {

    /**
     * all the fields needed in this class
     */
    private final String name;

    /**
     * A function to generate coordinates for battleship depends on the placement
     * @param placement the input placement including upperLeft and orientation
     * @return the set of coordinates of a battleship
     *   b      OR    b         bbb         b
     *               bbb           bb   OR    b     OR  bb
     *                             b                     b
     *
     *                Up          Right      Down      Left
     */
    public static HashSet<Coordinate> makeCoord(Placement placement){
        //initialize the variables
        Coordinate upperLeft = placement.getWhere();
        Character orientation = placement.getOrientation();
        int row = upperLeft.getRow();
        int column = upperLeft.getColumn();
        HashSet<Coordinate> set = new HashSet<>();//the output

        //catch the exception
        if(orientation.equals('V') || orientation.equals('H')) {
            throw new IllegalArgumentException(
                    "the orientation of this type of ship must be 'U' or 'u' or 'D' or 'd' or 'L' or 'l' or 'R' or 'r' but is "  + orientation
            );
        }
        //generate coordinates
        //up
        if(orientation.equals('U')){
            set.add(new Coordinate(row,column + 1));
            set.add(new Coordinate(row+1,column));
            set.add(new Coordinate(row + 1,column+1));
            set.add(new Coordinate(row + 1,column+2));
        }
        //right
        if (orientation.equals('R')){
            set.add(new Coordinate(row,column));
            set.add(new Coordinate(row+1,column));
            set.add(new Coordinate(row+1,column+1));
            set.add(new Coordinate(row+2,column));
        }
        //left
        if (orientation.equals('L')){
            set.add(new Coordinate(row+1,column));
            set.add(new Coordinate(row,column+1));
            set.add(new Coordinate(row+1,column+1));
            set.add(new Coordinate(row+2,column+1));
        }
        //down
        if (orientation.equals('D')) {
            set.add(new Coordinate(row,column));
            set.add(new Coordinate(row,column+1));
            set.add(new Coordinate(row,column+2));
            set.add(new Coordinate(row+1,column+1));
        }
        return set;
    }
    public TShapeShip(String name, Placement place,
                      SimpleShipDisplayInfo<T> selfDisplayInfo, SimpleShipDisplayInfo<T> enemyDisplayInfo,Character orientation) {
        super(makeCoord(place), selfDisplayInfo, enemyDisplayInfo,makeMapCoord(place.getWhere(),orientation,name));
        this.name = name;
    }

    public TShapeShip(String name, Placement p, T data, T onHit,Character orientation) {
        this(name, p, new SimpleShipDisplayInfo<>(data, onHit), new SimpleShipDisplayInfo<>(null, data),orientation);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Ship fireAt(Coordinate c) {
        return null;
    }

    /**
     * make the hashmap of coordinate for battleship
     * @param upperLeft
     * @param orientation
     * @param name
     * @return
     */
    static HashMap<Coordinate, Integer> makeMapCoord(Coordinate upperLeft, Character orientation, String name){
        HashMap<Coordinate, Integer> tmp = new HashMap<>();
        int row = upperLeft.getRow();
        int column = upperLeft.getColumn();
            if (orientation == 'U' || orientation == 'u') {
                tmp.put(new Coordinate(row,column+1),1);
                tmp.put(new Coordinate(row+1,column),2);
                tmp.put(new Coordinate(row+1,column+1),3);
                tmp.put(new Coordinate(row+1,column+2),4);
            }
            if (orientation == 'R' || orientation == 'r') {
                tmp.put(new Coordinate(row+1,column+1),1);
                tmp.put(new Coordinate(row,column),2);
                tmp.put(new Coordinate(row+1,column),3);
                tmp.put(new Coordinate(row+2,column),4);
            }
            if (orientation == 'D' || orientation == 'd'){
                tmp.put(new Coordinate(row+1,column+1),1);
                tmp.put(new Coordinate(row,column+2),2);
                tmp.put(new Coordinate(row,column+1),3);
                tmp.put(new Coordinate(row,column),4);
            }
            if (orientation == 'L' || orientation == 'l') {
                tmp.put(new Coordinate(row+1,column),1);
                tmp.put(new Coordinate(row,column+1),4);
                tmp.put(new Coordinate(row+1,column+1),3);
                tmp.put(new Coordinate(row+2,column+1),2);
            }

        return tmp;

    }


}
