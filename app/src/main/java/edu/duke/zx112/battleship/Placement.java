package edu.duke.zx112.battleship;

public class Placement {
    private final Coordinate where;
    private final char orientation;

    /**
     * Construct a Placement with the specified coordinate and orientation
     * @param where is the index of the coordinate
     * @param orientation is the direction of this newly constructed Placement
     */
    public Placement(Coordinate where, char orientation) {
        if(orientation >= 'a' && orientation <= 'z') {
            orientation = (char) (orientation - 32);
        }
        //UPDATE 2/13: changes to placement checker for different orientation for battleship and carrier
        if(orientation != 'V' && orientation != 'H'&& orientation != 'U' && orientation != 'R' && orientation != 'D' && orientation != 'L'
        ) {
            throw new IllegalArgumentException(
                    "orientation must be 'V' or 'v' or 'H' or 'h' U' or 'u' or 'D' or 'd' or 'L' or 'l' or 'R' or 'r' but is " + orientation
            );
        }
        this.where = where;
        this.orientation = orientation;
    }

    /**
     * Construct a Placement with the specified coordinate and orientation
     * @param desc is a string like "A2V"
     */
    public Placement(String desc) {
        if(desc.length() != 3 ) {
            throw new IllegalArgumentException("That placement is invalid: it does not have the correct format." + desc);
        }
        desc = desc.toUpperCase();
        String coordinateString = desc.substring(0,2);
        Coordinate where = new Coordinate(coordinateString);
        char orientation = desc.charAt(2);
        if(orientation != 'V' && orientation != 'H' && orientation != 'U' &&
                orientation != 'R' && orientation != 'D' && orientation != 'L') {

            throw new IllegalArgumentException("That placement is invalid: it does not have the correct format.");
        }
        this.where = where;
        this.orientation = orientation;
    }

    public char getOrientation() {
        return orientation;
    }

    public Coordinate getWhere() {
        return where;
    }

    @Override
    public boolean equals(Object o) {
        if(o.getClass().equals(getClass())) {
            Placement p = (Placement) o;
            return orientation == p.orientation && where.equals(p.where);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public String toString() {
        String orientationString = orientation == 'H'? "Horizontal" : "Vertical";
        return where.toString() + " " + orientationString;
    }
}


