package edu.duke.zx112.battleship;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.InvalidPropertiesFormatException;
import java.util.function.Function;

public class TextPlayer {
    private final AbstractShipFactory<Character> shipFactory1;
    private final AbstractShipFactory<Character> shipFactory2;
    final Board<Character> theBoard;
    final BoardTextView view;
    private final BufferedReader inputReader;
    private final PrintStream out;
    final String name;
    public int numMove = 3;
    public int numSonar = 3;
    private final ArrayList<String> shipsToPlace;
    final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;

    /**
     * this function initialize a player and pass all the parameters we need
     * @param name
     * @param board
     * @param view
     * @param inputReader
     * @param out
     * @param
     */
    public TextPlayer(String name, Board<Character> board, BoardTextView view,
                      BufferedReader inputReader, PrintStream out, V1ShipFactory shipFactory1,V2ShipFactory shipFactory2) {
        this.name = name;
        this.theBoard = board;
        this.view = view;
        this.inputReader = inputReader;
        this.out = out;
        this.shipFactory1 = shipFactory1;
        this.shipFactory2=shipFactory2;
        this.shipsToPlace = new ArrayList<>();
        this.shipCreationFns = new HashMap<>();
        setupShipCreationMap();
        setupShipCreationList();
    }

    /**
     * Print out a prompt and read the placement from the inputReader and return newly constructed placement
     * @param prompt is the prompt message which want to print out
     * @return newly constructed placement
     * @throws IOException when reading errors happen
     */
    public Placement readPlacement(String prompt) throws IOException {
        out.println(prompt);
        String s = inputReader.readLine();
        return new Placement(s);
    }

    /**
     * Take an input to construct a Placement and place a ship to the board and
     * print out theBoard
     * @throws IOException when encountering some reading and printing errors
     */
    public void doOnePlacement(String shipName) throws IOException {
        while(true){
            try{
                while(true){
                    Placement p = readPlacement("Player " + name + " where do you want to place a " + shipName + "?");
                    Ship<Character> s = shipCreationFns.get(shipName).apply(p);
                    if(theBoard.tryAddShip(s)!=null){
                        out.println(theBoard.tryAddShip(s));
                    }else{
                        break;
                    }
                }
                BoardTextView view=new BoardTextView(theBoard);
                out.println("Current ocean:");
                out.print(view.displayMyOwnBoard());
                break;
            }catch(IllegalArgumentException e){
                out.println("The placement is invalid! It does not have a correct format!");
            }
        }

    }

    /**
     * this function place one player's ships on the board
     * @throws IOException
     */
    public void doPlacementPhase() throws IOException {
        for(String shipName: shipsToPlace) {
            out.println("--------------------------------------------------------------------------------");
            out.println(view.displayMyOwnBoard());
            out.println("---------------------------------------------------------------------------\n" +
                    "Player " + name + ": you are going to place the following ships (which are all\n" +
                    "rectangular). For each ship, type the coordinate of the upper left\n" +
                    "side of the ship, followed by either H (for horizontal) or V (for\n" +
                    "vertical).  For example M4H would place a ship horizontally starting\n" +
                    "at M4 and going to the right.  You have\n" +
                    "\n" +
                    "2 \"Submarines\" ships that are 1x2 \n" +
                    "3 \"Destroyers\" that are 1x3\n" +
                    "3 \"Battleships\" that are 1x4\n" +
                    "2 \"Carriers\" that are 1x6\n" +
                    "---------------------------------------------------------------------------");
            this.doOnePlacement(shipName);
        }

    }

    /**
     * set up all the ships one player need
     */
    protected void setupShipCreationMap(){
        shipCreationFns.put("Submarine", (p) -> shipFactory1.makeSubmarine(p));
        shipCreationFns.put("Destroyer", (p) -> shipFactory1.makeDestroyer(p));
        shipCreationFns.put("Battleship", (p) -> shipFactory2.makeBattleship(p));
        shipCreationFns.put("Carrier", (p) -> shipFactory2.makeCarrier(p));


    }

    protected void setupShipCreationList(){
        shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
        shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
        shipsToPlace.addAll(Collections.nCopies(3, "Battleship"));
        shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));

    }

    /**
     * play one turn
     */
    public void playOneTurn(Board<Character> enemyBoard, String enemyName) throws IOException {
        String prompt = "Player " + this.name + "'s turn";
        out.println(prompt);
        BoardTextView enemyBoardTextView = new BoardTextView(enemyBoard);
        out.println(view.displayMyBoardWithEnemyNextToIt(enemyBoardTextView, "my board", "enemy board"));
        readCommand(enemyBoard,enemyName);

    }

    /**
     * for player to fire
     * @param enemyBoard
     * @param enemyName
     * @throws IOException
     */
    public void playOneTurn_fire(Board<Character> enemyBoard, String enemyName) throws IOException {
        String prompt = "Player " + this.name + " Where would you like to fire at?";
        Coordinate fireCoordinate;
        while (true) {
            try {
                fireCoordinate = readCoordinate(prompt);
                break;
            } catch (IllegalArgumentException e) {
                out.println(e.getMessage());
            }
        }
        Ship<Character> fireShip = enemyBoard.fireAt(fireCoordinate);
        if (fireShip == null) {
            out.println("You missed!");
        }
    }

    /**
     * read the coordinate input from user
     * @param prompt    the coordinate input from user
     * @return
     * @throws IOException if the input is null
     */
    public Coordinate readCoordinate(String prompt) throws IOException {
        Coordinate c = null;
        System.out.println(prompt);
        String s = inputReader.readLine();
        try {
            c = new Coordinate(s);
            return c;
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Coordinate is invalid, please enter the new coordinate again");
        }
    }

    public boolean check_lost(){
        return theBoard.allSunk();
    }

    /**
     * check the choice is valid or not
     * @return
     * @throws IOException
     */
    public String Read_Choice() throws IOException {
        String prompt = "Possible actions for Player " + name + " :\n\n" +
                " F Fire at a square\n" +
                " M Move a ship to another square (" + numMove + " remaining)\n" +
                " S Sonar scan (" + numSonar + " remaining)\n\n" +
                "Player " + name + ", what would you like to do?\n";
        out.println(prompt);
        String s = null;
        while (true) {
            try {
                s = inputReader.readLine();
                if (s == null) {
                    throw new IllegalArgumentException("The input is null\n");
                }
                if (!(s.equals("F") || s.equals("M") || s.equals("S"))) {
                    throw new IllegalArgumentException("Please enter a valid choice\n");
                }
                if(numMove == 0 && s.equals("M")){
                    throw new IllegalArgumentException("M has 0 remaining, enter again please\n");
                }
                if(numSonar == 0 && s.equals("S")){
                    throw new IllegalArgumentException("S has 0 remaining, enter again please\n");
                }
                break;
            } catch (IllegalArgumentException e) {
                out.println(e.getMessage());
            }
        }
        return s;
    }

    /**
     * read command from user
     * @param enemyBoard
     * @param enemyName
     * @throws IOException
     */
    public void readCommand(Board<Character> enemyBoard, String enemyName) throws IOException {
        String choice = Read_Choice();
        if (choice.equals("F")) {
            playOneTurn_fire(enemyBoard,enemyName );
        } else if (choice.equals("M")) {
            if (!playOneTurn_move()) {
                readCommand(enemyBoard,enemyName);
            } else {
                out.println("Successfully move!\n");
                numMove --;
            }
        }
        else {
            playOneTurn_scan(enemyBoard);
            numSonar --;
        }
    }

    /**
     * implement move function 
     * @return
     * @throws IOException
     */
    public boolean playOneTurn_move() throws IOException {
        String prompt = "Which ship do you want to do move?";
        Coordinate choose = null;
        Ship<Character> old_ship = null;
        while (true) {
            try {
                choose = readCoordinate(prompt);
                old_ship = theBoard.get_Ship(choose);
                if (old_ship != null) {
                    break;
                }
                else {
                    throw new InvalidPropertiesFormatException("There is no ship here, please enter again: ");
                }
            } catch (Exception e) {
                out.println(e.getMessage());
            }
        }
        String ship_name = old_ship.getName();
        String move_prompt = "Where do you want to place the moved ship?";
        Placement p = null;
        Ship<Character> new_ship_add;
        String str;
        while(true){
            try {
                p = readPlacement(move_prompt);
                new_ship_add= shipCreationFns.get(ship_name).apply(p);
                str = theBoard.tryAddShip(new_ship_add);
                break;
            }
            catch (IllegalArgumentException e){
                out.println(e.getMessage());
                return false;
            }

        }
        if(str != null){
            return  false;
        }
        theBoard.removeAndResetShip(old_ship, new_ship_add);
        return true;
    }

    /**
     * v2 task 2b
     */

    /**
     *implement the scan function 
     * @param enermyBoard
     * @throws IOException
     */
    private void playOneTurn_scan(Board<Character> enermyBoard) throws IOException{
        String prompt = "Player " + this.name + ", Please specify the coordinate you want to scan";
        Coordinate coordinate = readCoordinate(prompt);
        HashMap<String, Integer> SonarMap =  enermyBoard.getScanMap(coordinate);

        System.out.println("Detect " +SonarMap.get("Submarine") + " coordinates of Submarine");
        System.out.println("Detect " + SonarMap.get("Destroyer") + " coordinates of Destroyer");
        System.out.println("Detect " + SonarMap.get("Battleship") + " coordinates of Battleship");
        System.out.println("Detect " + SonarMap.get("Carrier") + " coordinates of Carrier");
    }




}
