package edu.duke.zx112.battleship;

import java.util.*;

public abstract class BasicShip<T> implements Ship<T> {
    protected ShipDisplayInfo<T> myDisplayInfo;
    protected HashMap<Coordinate, Boolean> myPieces;
    protected ShipDisplayInfo<T> enemyDisplayInfo;
    /**
     * v2: two variables one is the order version for coordinates, another records the hit list
     */
    protected HashMap<Coordinate, Integer> myPieces_v2;

    public BasicShip(Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo, HashMap<Coordinate, Integer> myPieces_v2){
        myPieces = new HashMap<Coordinate,Boolean>();
        this.myDisplayInfo = myDisplayInfo;
        this.enemyDisplayInfo = enemyDisplayInfo;
        for(Coordinate c : where){
            myPieces.put(c,false);
        }
        this.myPieces_v2 = myPieces_v2;
    }
    public HashMap<Coordinate, Integer> getmyPieces_v2(){
        return myPieces_v2;
    }
    public List<Integer> getOrder_hit(){
        List<Integer> Order_hit = new ArrayList<>();
        for (Coordinate c : myPieces.keySet()){
            if (myPieces.get(c) == true){
                Order_hit.add(myPieces_v2.get(c));
            }
        }
        return Order_hit;
    }


    @Override
    public boolean occupiesCoordinates(Coordinate where) {
        return myPieces.containsKey(where);
    }

    protected void checkCoordinateInThisShip(Coordinate c){
        if(occupiesCoordinates(c) == false){
            throw new IllegalArgumentException("Coordinate is not on the ship\n");
        }
    }
    /**
     * check if the ship sunk by loop the hashmap
     * @return
     */
    @Override
    public boolean isSunk() {
        for (Coordinate c: myPieces.keySet()){
            //means one coordinate of ship is no hit
            if(wasHitAt(c) == false){
                return  false;
            }
        }
        return true;
    }

    /**
     * record we hit a ship by change the key's value
     * @param where specifies the coordinates that were hit.
     */
    @Override
    public void recordHitAt(Coordinate where) {
        //first we have to check whether the coordinate is on ship
        checkCoordinateInThisShip(where);
        myPieces.put(where,true);
    }

    /**
     * Check if a coordinate is hit
     * @param where is the coordinates to check.
     * @return
     */
    @Override
    public boolean wasHitAt(Coordinate where) {
        //first whether the coordinate is on the ship
        checkCoordinateInThisShip(where);
        return myPieces.get(where);
    }


    @Override
    public T getDisplayInfoAt(Coordinate where,boolean myShip) {
        if (myShip == true){
            return myDisplayInfo.getInfo(where, wasHitAt(where));
        }
        return enemyDisplayInfo.getInfo(where,wasHitAt(where));


    }

    /**
     * Get all of the Coordinates that this Ship occupies.
     *
     * @return An Iterable with the coordinates that this Ship occupies
     */
    public Iterable<Coordinate> getCoordinates() {
        return myPieces.keySet();
    }


    public ArrayList<Integer> markHits() {
        ArrayList<Integer> hits = new ArrayList<>();
        for(Coordinate c: getCoordinates()) {
            if(wasHitAt(c)) {
                hits.add(myPieces_v2.get(c));
            }
        }
        return hits;
    }


    public void restoreHits(ArrayList<Integer> hitIndexes) {
        for (Integer hit : hitIndexes) {
            for (Coordinate c : getCoordinates()) {
                if (myPieces_v2.get(c) == hit) recordHitAt(c);
            }
        }
    }

}

