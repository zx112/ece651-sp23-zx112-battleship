package edu.duke.zx112.battleship;
import java.util.*;
public interface Board<T> {
    public void removeAndResetShip(Ship<T> shipTarget, Ship<T> newShip);
    HashMap<String, Integer> getScanMap(Coordinate coordinate);

    public Ship<T> get_Ship (Coordinate where);
    public void tryRemove(Ship<T> old_ship, Ship<T>new_ship);
    int getWidth();

    int getHeight();

    /**
     * Add the ship to the list and return true.
     * Check the validity of the placement and returns true if the
     * placement was OK and false otherwise.
     * @param toAdd is the ship to be added
     * @return true if the add action is correctly done, false otherwise
     */
    String tryAddShip(Ship<T> toAdd);


    /**
     * It takes a Coordinate, and sees which Ship occupies that coordinate.
     * If one is found, we return whatever displayInfo it has at those coordinates.
     * If none, return null.
     * @param where is the specified coordinate to check what it is
     * @return return whatever displayInfo it has at those coordinates
     */
    T whatIsAt(Coordinate where);
    public Ship<T> fireAt(Coordinate c);
    public T whatIsAtForSelf(Coordinate where);
    T whatIsAtForEnemy(Coordinate where);
    boolean isLose();

    boolean allSunk();

}
