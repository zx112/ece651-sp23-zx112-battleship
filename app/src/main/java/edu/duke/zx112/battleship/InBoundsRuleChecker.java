package edu.duke.zx112.battleship;

public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T> {
    public InBoundsRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }

    /**
     * Check if the ship's coordinates is inside the board
     * @param theShip
     * @param theBoard
     * @return
     */
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        //get the board bounder
        int max_width = theBoard.getWidth();
        int max_height = theBoard.getHeight();
        //loop all the coordinates in the ship
        for(Coordinate c : theShip.getCoordinates()){
            if(c.getRow() >= max_height ) {
                return "That placement is invalid: the ship goes off the bottom of the board.";
            } else if(c.getColumn() >= max_width) {
                return "That placement is invalid: the ship goes off the right of the board.";
            } else if(c.getColumn() < 0) {
                return "That placement is invalid: the ship goes off the left of the board.";
            } else if(c.getRow() < 0) {
                return "That placement is invalid: the ship goes off the top of the board.";

            }
        }
        return null;
    }

}
