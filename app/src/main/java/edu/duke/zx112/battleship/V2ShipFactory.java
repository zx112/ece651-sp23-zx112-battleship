package edu.duke.zx112.battleship;

/**
 * Ship factory for version2
 */
public class V2ShipFactory extends V1ShipFactory {

    protected Ship<Character> createShip(Placement where, Character letter, String name) {
        if (name.equals("Battleship")){
            return new TShapeShip<>(name, where,letter,  '*',where.getOrientation());
        } else if (name.equals("Carrier")) {
            return new ZShapeShip<>(name, where,letter,  '*',where.getOrientation());
        }else {
            throw new IllegalArgumentException("You should make battleship or carrier but you made:"+ name);
        }


    }

    @Override
    public Ship<Character> makeBattleship(Placement where) {
        return createShip(where,'b',"Battleship");
    }

    @Override
    public Ship<Character> makeCarrier(Placement where) {
        return createShip(where,'c',"Carrier");
    }
}
