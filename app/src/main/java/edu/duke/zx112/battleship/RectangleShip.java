package edu.duke.zx112.battleship;
import java.util.*;
public class RectangleShip<T> extends BasicShip<T> {
    public RectangleShip(String name, Coordinate upperLeft, int width, int height, ShipDisplayInfo<T> myDisplayInfo,ShipDisplayInfo<T> enemyDisplayInfo,Character orientation) {
        super(makeCoords(upperLeft, width, height), myDisplayInfo,enemyDisplayInfo,makeMapCoord(upperLeft,orientation,name));
        this.name = name;
    }

    public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit,Character orientation) {
        this(name, upperLeft, width, height, new SimpleShipDisplayInfo<T>(data, onHit), new SimpleShipDisplayInfo<>(null,data),orientation);
    }
    public RectangleShip(Coordinate upperLeft, T data, T onHit) {
        this("testship", upperLeft, 1, 1, data, onHit,'V');
    }


    /**
     * This method should generate the set of coordinates for a rectangle starting
     * at upperLeft whose width and height are as specified. E.g. if
     * upperLeft = (row=1,col=2)
     * width = 1
     * height = 3
     *
     * You would return the set {(row=1,col=2), (row=2,col=2), (row=3,col=2)}
     * @param upperLeft
     * @param width
     * @param height
     * @return
     */
    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height){
        HashSet<Coordinate> set = new HashSet<>();
        set.add(upperLeft);
        int row = upperLeft.getRow();
        int column = upperLeft.getColumn();
        for (int i = 0; i < height; i ++){
            for (int j = 0; j < width; j ++){
                set.add(new Coordinate(i + row,j+column));
            }
        }
        return set;
    }

    private final String name;
    @Override
    public String getName() {
        return name;
    }

    /**
     * Get all the Coordinates that this Ship occupies.
     *
     * @return An Iterable with the coordinates that this Ship occupies
     */
    public Iterable<Coordinate> getCoordinates() {
        return myPieces.keySet();
    }

    @Override
    public Ship<T> fireAt(Coordinate c) {
        return null;
    }

    /**
     * v2: make the coordinates mapping to a index, for this class we only have s and d
     * @param upperLeft
     * @param orientation
     * @param name
     * @return
     */
    static HashMap<Coordinate, Integer> makeMapCoord(Coordinate upperLeft, Character orientation, String name){
        HashMap<Coordinate, Integer> tmp = new HashMap<>();
        int row = upperLeft.getRow();
        int column = upperLeft.getColumn();
        //for v2: check the orientation for rectangle ships
        if(orientation.equals('U') || orientation.equals('D')|| orientation.equals('R')|| orientation.equals('L')) {
            throw new IllegalArgumentException(
                    "the orientation of this type of ship must be vertical or horizontal but is "  + orientation
            );
        }
        if(name.equals("Submarine")) {
            if (orientation == 'v' || orientation == 'V'){
                tmp.put(new Coordinate(row,column),1);
                tmp.put(new Coordinate(row+1,column),2);
            }
            if (orientation == 'h' || orientation == 'H'){
                tmp.put(new Coordinate(row,column),1);
                tmp.put(new Coordinate(row,column+1),2);
            }
        }
        if(name.equals("Destroyer")) {
            if (orientation == 'v' || orientation == 'V'){
                tmp.put(new Coordinate(row,column),1);
                tmp.put(new Coordinate(row+1,column),2);
                tmp.put(new Coordinate(row+2,column),3);
            }
            if (orientation == 'h' || orientation == 'H'){
                tmp.put(new Coordinate(row,column),1);
                tmp.put(new Coordinate(row,column+1),2);
                tmp.put(new Coordinate(row,column+2),3);
            }
        }
        return tmp;

    }

}
