package edu.duke.zx112.battleship;

import java.util.function.Function;

/**
 * This class handles textual display of a Board (i.e., converting it to a
 * string to show to the user).
 * It supports two ways to display the Board:
 * one for the player's own board,
 * one for the enemy's board.
 */
public class BoardTextView {
    /**
     * The Board to display
     */
    private final Board<Character> toDisplay;

    /**
     * Construct a BoardView, given the board it will display.
     * @param toDisplay is the board to display
     * @throws IllegalArgumentException if the board is larger than 10 x 26.
     */
    public BoardTextView(Board<Character> toDisplay) {
        this.toDisplay = toDisplay;
        if(toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
            throw new IllegalArgumentException(
                    "Board must be no larger than 10x26, but is " + toDisplay.getWidth()
                            + "x" + toDisplay.getHeight()
            );
        }
    }

    /**
     * This makes the view of my own board, e.g.
     *   0|1|2|3|4|5|6|7|8|9
     * A  | | | | | | | | |  A
     * B  | | | | | | | | |  B
     * C  | | | | | | | | |  C
     * D  | | | | | | | | |  D
     * E  | | | | | | | | |  E
     * F  | | | | | | | | |  F
     * G  | | | | | | | | |  G
     * H  | | | | | | | | |  H
     * I  | | | | | | | | |  I
     * J  | | | | | | | | |  J
     * K  | | | | | | | | |  K
     * L  | | | | | | | | |  L
     * M  | | | | | | | | |  M
     * N  | | | | | | | | |  N
     * O  | | | | | | | | |  O
     * P  | | | | | | | | |  P
     * Q  | | | | | | | | |  Q
     * R  | | | | | | | | |  R
     * S  | | | | | | | | |  S
     * T  | | | | | | | | |  T
     *   0|1|2|3|4|5|6|7|8|9
     * @return the String of my own board
     */
    protected String displayAnyBoard(Function<Coordinate, Character> getSquareFn) {
        int width = toDisplay.getWidth();
        int height = toDisplay.getHeight();
        String header = makeHeader();
        StringBuilder board = new StringBuilder();
        board.append(header);

        for(int row = 0; row < height; row++) {
            char rowChar = (char) ('A' + row);
            String leftBound = rowChar + " ";
            board.append(leftBound);
            String sep = "";
            for(int column = 0; column < width; column++) {
                board.append(sep);
                Coordinate c = new Coordinate(row, column);
                Character elem = getSquareFn.apply(c);
                board.append(elem == null? ' ': elem);
                sep = "|";
            }
            String rightBound = " " + rowChar + "\n";
            board.append(rightBound);
        }
        board.append(header);
        return board.toString(); // this is a placeholder for the moment
    }
    public String displayMyOwnBoard() {
        return displayAnyBoard((c)->toDisplay.whatIsAtForSelf(c));
    }
    public String displayEnemyBoard() {
        return displayAnyBoard(toDisplay::whatIsAtForEnemy);
    }



    /**
     * This makes the header line e.g. 0|1|2|3|4\n
     * @return the String that is the header line for the given board
     */
    String makeHeader() {
        StringBuilder ans = new StringBuilder("  "); // README shows two spaces
        String sep = ""; // start with nothing to separate, then switch to | to separate
        for(int i = 0; i < toDisplay.getWidth(); i++) {
            ans.append(sep);
            ans.append(i);
            sep = "|";
        }
        ans.append("\n");
        return ans.toString();
    }
    /**
     * method to display the whole board
     * @param enemyView
     * @param myHeader
     * @param enemyHeader
     * @return
     */
    public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) {
        //initialize two string[] for split
        String[] mystr = this.displayMyOwnBoard().split("\n");
        String[] enemystr = enemyView.displayEnemyBoard().split("\n");
        //the output
        StringBuilder output = new StringBuilder();
        int w = toDisplay.getWidth();
        int first_head = 5;
        int distance = 2 * w + 22 - first_head - myHeader.length();
        int myspace = 2 * w + 19 - first_head - myHeader.length();

        //generate the board
        output.append("\n");
        output.append("     ");//five space
        output.append(myHeader);
        for (int i = 0; i < distance; i ++){output.append(" ");}
        output.append(enemyHeader);
        output.append("\n");//done for header

        output.append(mystr[0]);
        for (int i = 0; i < myspace + 2 ; i ++) output.append(" ");
        output.append(enemystr[0]);
        output.append("\n");//done for line1

        //now loop
        for (int i = 1; i <mystr.length - 1; i ++){
            output.append(mystr[i]);
            for (int j = 0; j < myspace; j ++) output.append(" ");
            output.append(enemystr[i]);
            output.append("\n");

        }

        //last line
        output.append(mystr[mystr.length-1]);
        for (int i = 0; i < myspace + 2 ; i ++) output.append(" ");
        output.append(enemystr[enemystr.length-1]);
        output.append("\n");



        return output.toString();

    }

}
