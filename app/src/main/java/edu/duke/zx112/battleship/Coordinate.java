package edu.duke.zx112.battleship;

public class Coordinate {
    private final int row;
    private final int column;

    /**
     * Construct a Coordinate with the specified row and column
     * @param row is the row index of the newly constructed coordinate
     * @param column is the column index of the newly constructed coordinate
     */
    public Coordinate(int row, int column) {
        if(row < 0 || row >= 20) {
            throw new IllegalArgumentException(
                    "Coordinate's row must be between 0 and 19 but is " + row
            );
        }
        if(column < 0 || column >= 10) {
            throw new IllegalArgumentException(
                    "Coordinate's column must be between 0 and 9 but is " + row
            );
        }
        this.row = row;
        this.column = column;
    }

    /**
     * Construct a Coordinate with the specified row and column
     * @param desc is a string like "A2" and makes the Coordinate
     * that corresponds to that string (e.g. row=0, column =2).
     */
    public Coordinate(String desc) {
        if(desc.length() != 2) {
            throw new IllegalArgumentException(
                    "Coordinate must be constructed by string whose length is 2 but is " + desc.length()
            );
        }
        desc = desc.toUpperCase();
        char rowLetter = desc.charAt(0);
        char columnLetter = desc.charAt(1);

        if(rowLetter < 'A' || rowLetter > 'Z') {
            throw new IllegalArgumentException(
                    "Coordinate's row must be between A and Z but is " + rowLetter
            );
        }
        if(columnLetter < '0' || columnLetter > '9') {
            throw new IllegalArgumentException(
                    "Coordinate's row must be between 0 and 9 but is " + columnLetter
            );
        }
        this.row = rowLetter - 'A';
        this.column = columnLetter - '0';
    }

    public int getColumn() {
        return column;
    }

    public int getRow() {
        return row;
    }

    @Override
    public boolean equals(Object o) {
        if(o.getClass().equals(getClass())) {
            Coordinate c = (Coordinate) o;
            return row == c.row && column == c.column;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }


    @Override
    public String toString() {
        return "(" + row + ", " + column + ")";
    }

}
