package edu.duke.zx112.battleship;

import java.util.*;

public class BattleShipBoard<T> implements Board<T> {
    private final int width;
    private final int height;

    private final ArrayList<Ship<T>> myShips;
    private final PlacementRuleChecker<T> placementChecker;
    HashSet<Coordinate> enemyMisses;
    final T missInfo;
    /**
     * v2:record enemy hit coordinates for movement use
     */
    private HashMap<Coordinate, T> enemyHit;

    /**
     * in order to determine is lose or not we find out whether all the ships issunk or not
     *
     * @return
     */
    public boolean isLose() {
        for (Ship<T> ship : myShips) {
            if (ship.isSunk() != true) return false;
        }
        return true;
    }

    public boolean allSunk() {
        boolean allSunk = true;
        for (Ship<T> ship : myShips) {
            if (!ship.isSunk()) {
                allSunk = false;
                break;
            }
        }
        return allSunk;
    }


    /**
     * Constructs a BattleShipBoard with the specified width and height
     *
     * @param w is the width of the newly constructed board
     * @param h is the height of the newly constructed board
     * @throws IllegalArgumentException if the width or height are less than or equal to 0
     */
    public BattleShipBoard(int w, int h, PlacementRuleChecker<T> checker, T missInfo) {
        this.placementChecker = checker;
        this.myShips = new ArrayList<>();
        if (w <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's width must be positive but is " + w);
        }
        if (h <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's height must be positive but is " + h);
        }
        this.width = w;
        this.height = h;
        this.enemyMisses = new HashSet<>();
        this.missInfo = missInfo;
        this.enemyHit = new HashMap<>();

    }

    public BattleShipBoard(int w, int h, T missInfo) {
        this(w, h, new InBoundsRuleChecker<T>(new NoCollisionRuleChecker<>(null)), missInfo);
    }

    /**
     * This method should search for any ship that occupies coordinate c
     * (you already have a method to check that)
     * If one is found, that Ship is "hit" by the attack and should
     * record it (you already have a method for that!).  Then we
     * should return this ship.
     *
     * @param c
     * @return
     */
    public Ship<T> fireAt(Coordinate c) {
        for (Ship<T> ship : myShips) {
            if (ship.occupiesCoordinates(c) == true) {
                ship.recordHitAt(c);
                System.out.println("You hit! " + ship.getName());
                return ship;
            }
        }
        System.out.println("You miss!");
        enemyMisses.add(c);
        return null;
    }


    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    /**
     * Add the ship to the list and return true.
     * Check the validity of the placement and returns true if the
     * placement was OK and false otherwise.
     *
     * @param toAdd is the ship to be added
     * @return true if the add action is correctly done, false otherwise
     */
    public String tryAddShip(Ship<T> toAdd) {
        String addException = placementChecker.checkPlacement(toAdd, this);
        if (addException == null) {
            myShips.add(toAdd);
            return null;
        } else {
            System.err.println(addException);
            return addException;
        }

    }

    @Override
    public T whatIsAt(Coordinate where) {
        return null;
    }

    /**
     * It takes a Coordinate, and sees which Ship occupies that coordinate.
     * If one is found, we return whatever displayInfo it has at those coordinates.
     * If none, return null.
     *
     * @param where is the specified coordinate to check what it is
     * @return return whatever displayInfo it has at those coordinates
     */

    protected T whatIsAt(Coordinate where, boolean isSelf) {
        for (Ship<T> s : myShips) {
            if (s.occupiesCoordinates(where)) {
                return s.getDisplayInfoAt(where, isSelf);
            }
        }
        return null;
    }

    public T whatIsAtForSelf(Coordinate where) {
        return whatIsAt(where, true);
    }

    public T whatIsAtForEnemy(Coordinate where) {
        T output = whatIsAt(where, false);
        if (output == null) {
            if (enemyHit.containsKey(where)) {
                return enemyHit.get(where);
            }
            if (enemyMisses.contains(where)) {
                return missInfo;
            }
        }
        return output;
    }

    /**
     * input a coordinate and return the corresponding ship on that coordinate or null
     *
     * @param where
     * @return
     */
    public Ship<T> get_Ship(Coordinate where) {
        for (Ship<T> s : myShips) {
            if (s.occupiesCoordinates(where)) {
                return s;
            }
        }
        return null;
    }


    /**
     * Remove ship from board
     *
     * @param old_ship the ship we want to remove
     * @param new_ship the new ship we add after removing
     */
    public void tryRemove(Ship<T> old_ship, Ship<T> new_ship) {
        for (Coordinate c : new_ship.getCoordinates()) {
            for (int i = 0; i < old_ship.getOrder_hit().size(); ++i) {
                if (new_ship.getmyPieces_v2().get(c) == old_ship.getOrder_hit().get(i)) {
                    new_ship.recordHitAt(c);
                }
            }
        }
        getEnemyHit(old_ship);
        myShips.remove(old_ship);
    }

    /**
     * record all the hit coordinates by enemy
     *
     * @param old_ship
     */
    public void getEnemyHit(Ship<T> old_ship) {
        for (Coordinate c : old_ship.getCoordinates()) {
            if (old_ship.wasHitAt(c) == true) {
                enemyHit.put(c, whatIsAtForEnemy(c));
            }
        }
    }


    /**
     * v2: task2b sonar detection
     */

    /**
     * Check whether a coordinate is on the board or not. For sonar center coordinate to separate
     *
     * @param row row of board
     * @param col column of board
     * @return true if in the bound
     */
    protected boolean CoordinateInBoundChecker(int row, int col) {
        return row < height && row >= 0 && col < width && col >= 0;
    }

    /**
     * an arraylist to record all the sonar coordinates created by center
     *
     * @param center center of sonar
     * @return all sonar coordinates
     */
    protected ArrayList<Coordinate> generateSonar(Coordinate center) {
        //the arraylist we return
        ArrayList<Coordinate> SonarList = new ArrayList<>();
        //center information
        int row = center.getRow();
        int column = center.getColumn();
        // Generate upper half of sonar
        for (int i = row - 3; i <= row; i++) {
            int colEnd = column + i - (row - 3);
            int colStart = column - i + (row - 3);
            for (int j = colStart; j <= colEnd; j++) {
                if (CoordinateInBoundChecker(i, j)) {
                    Coordinate coordinate = new Coordinate(i, j);
                    SonarList.add(coordinate);
                }
            }
        }
        // Generate lower half of sonar
        for (int i = row + 1; i <= row + 3; i++) {
            int colEnd = column + (row + 3 - i);
            int colStart = column - (row + 3 - i);
            for (int j = colStart; j <= colEnd; j++) {
                if (CoordinateInBoundChecker(i, j)) {
                    Coordinate coordinate = new Coordinate(i, j);
                    SonarList.add(coordinate);
                }
            }
        }

        return SonarList;
    }

    /**
     * reset the remove ship from board
     *
     * @param shipTarget
     * @param newShip
     */
    @Override
    public void removeAndResetShip(Ship<T> shipTarget, Ship<T> newShip) {
        ArrayList<Integer> hitIndexes = shipTarget.markHits();
        newShip.restoreHits(hitIndexes);
        myShips.remove(shipTarget);

    }

    /**
     * Scan all the coordinate given by above function and record the result in a hashmap
     *
     * @param coordinate
     * @return
     */
    public HashMap<String, Integer> getScanMap(Coordinate coordinate) {
        ArrayList<Coordinate> scanArray = generateSonar(coordinate);
        System.out.println(scanArray);
        HashMap<String, Integer> scanMap = new HashMap<>();
        int numBattleship = 0,numCarrier = 0, numDestroyer=0,numSubmarine=0;

        for (Coordinate c : scanArray) {
            Ship<T> ship = get_Ship(c);
            if (ship != null) {
                String shipName = ship.getName();
                if (shipName.equals("Battleship")) {
                    numBattleship++;
                } else if (shipName.equals("Carrier")) {
                    numCarrier++;
                } else if (shipName.equals("Destroyer")) {
                    numDestroyer++;
                } else if (shipName.equals("Submarine")) {
                    numSubmarine++;
                }
            }
        }

        scanMap.put("Battleship", numBattleship);
        scanMap.put("Carrier", numCarrier);
        scanMap.put("Destroyer", numDestroyer);
        scanMap.put("Submarine", numSubmarine);

        return scanMap;


    }
}






